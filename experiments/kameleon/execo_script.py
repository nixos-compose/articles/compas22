from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep
import execo_engine

import sys
import os
import os.path as op
import yaml
import shutil
import time

class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--night', action='store_true', help='either to submit jobs at g5k night')
        parser.add_argument('--recipe', help='yaml file')
        parser.add_argument('--iterations', default=1, help='nb of iters')

    def init(self):
        nb_nodes = 1
        site = "grenoble"
        cluster = "dahu"
        job_time_type = "day"

        self.current_path = os.getcwd()

        if self.args.night:
            job_time_type = "night"

        self.iterations = int(self.args.iterations)

        with open(self.args.recipe, "r") as recipe_file:
            recipe = yaml.safe_load(recipe_file)
        # Adding hello to the recipe
        recipe["setup"].append({'adding_hello': [{'microstep1': [{'exec_in': "apt-get update && apt-get install -y hello"}]}]})
        # Saving the hello recipe
        with open(os.path.join(self.current_path, "hello_image.yaml"), "w") as hello_image:
            yaml.dump(recipe, hello_image)
        oar_job = reserve_nodes(nb_nodes, site, cluster, walltime=2*60*60, job_time_type=job_time_type)
        self.oar_job_id, self.site = oar_job[0]


    def run(self):
        build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]

        csv_file = open(os.path.join(self.current_path, f"data/data_{self.oar_job_id}.csv"), "w")
        csv_file.write("image, flavour, time, size\n")

        for i in range(self.iterations):
            execo_engine.log.logger.info(f"Starting iteration {i + 1}/{self.iterations}")

            execo_engine.log.logger.info("Removind the 'build' folder")
            if os.path.exists(os.path.join(self.current_path, "empty_image")):
                shutil.rmtree(os.path.join(self.current_path, "empty_image"))
            if os.path.exists(os.path.join(self.current_path, "hello_image")):
                shutil.rmtree(os.path.join(self.current_path, "hello_image"))

            execo_engine.log.logger.info("Building the 'empty' image")
            (name, duration, size) = self.build_and_get_stats("empty_image", build_node)
            csv_file.write(f"empty, kameleon, {duration}, {size}\n")
            csv_file.flush()
            execo_engine.log.logger.info("Building the 'hello' image")
            (name, duration, size) = self.build_and_get_stats("hello_image", build_node)
            csv_file.write(f"hello, kameleon, {duration}, {size}\n")
            csv_file.flush()
        csv_file.close()

        execo_engine.log.logger.info("Giving back the resources")
        oardel([(self.oar_job_id, self.site)])

    def build_and_get_stats(self, image_name, build_node):
        build_cmd = f"kameleon build {os.path.join(self.current_path, image_name)}.yaml -b {self.current_path} -s"
        build_remote = Remote(build_cmd, build_node)
        start_time = time.time()
        build_remote.run()
        end_time = time.time()
        artifact_size = get_folder_size(os.path.join(self.current_path, f"{image_name}"))
        execo_engine.log.logger.info(f"Built {image_name} in {end_time - start_time} resulting in {artifact_size} bytes")
        return (image_name, end_time - start_time, artifact_size)

def reserve_nodes(nb_nodes, site, cluster, walltime=3600, job_time_type="day"):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh", job_time_type]), site)])
    return jobs

def get_folder_size(folder):
	size = 0
	for path, dirs, files in os.walk(folder):
		for f in files:
			fp = os.path.join(path, f)
			size += os.stat(fp).st_size
	return size

if __name__ == "__main__":
    ENGINE = MyEngine()
    ENGINE.start()
