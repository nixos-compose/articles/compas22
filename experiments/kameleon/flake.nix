{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.11";
    mach-nix.url = "github:DavHau/mach-nix";
  };

  outputs = { self, nixpkgs, mach-nix }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    myPython = mach-nix.lib.${system}.mkPython {
      requirements = ''
        execo
        requests
        pyyaml
        '';
    };
  in
  {
    packages.${system} = {
      build_kameleon = pkgs.writeScriptBin "build_kameleon" ''
          ${myPython}/bin/python3 ${./execo_script.py} $@
      '';
    };
    defaultPackage.${system} = self.packages.${system}.build_kameleon;
    devShell.${system} = pkgs.mkShell {
      buildInputs = [ myPython ];
    };
  };
}
