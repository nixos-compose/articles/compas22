from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo, get_build_node, build_derivation

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os

class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_folder', default=f"{os.getcwd()}", help='Path to the nxc folder')
        parser.add_argument('--night', action='store_true', help='either to submit jobs at g5k night')
        parser.add_argument('--nix-chroot-script', help='path to the nix user chroot script, fetch from github otherwise')
        parser.add_argument('--iterations', default=5, help='nb of iters')
        parser.add_argument('--flavour', default="g5k-ramdisk", help='flavour to build')

    def init(self):
        nb_nodes = 1
        site = "grenoble"
        cluster = "dahu"
        job_time_type = "day"

        if self.args.night:
            job_time_type = "night"

        nb_iters = int(self.args.iterations)
        flavour = self.args.flavour
        nxc_path = self.args.nxc_folder
        chroot_script = self.args.nix_chroot_script

        nxc_build_file = None

        (job_id, build_node) = get_build_node(site, cluster, [job_time_type], walltime=60*60)

        csv_file = open(os.path.join(os.getcwd(), f"data/data_{job_id}.csv"), "w")
        csv_file.write("image, flavour, time, size\n")

        for _ in range(nb_iters):
            print("Building 'empty'")
            (_, time_empty, size_empty) = build_derivation(build_node, nxc_path, flavour, "empty", chroot_script, True)
            csv_file.write(f"empty, {flavour}, {time_empty}, {size_empty}\n")
            csv_file.flush()
            print("Building 'hello'")
            (_, time_hello, size_hello) =  build_derivation(build_node, nxc_path, flavour, "hello", chroot_script, False)
            print("Building 'hello'")
            csv_file.write(f"hello, {flavour}, {time_hello}, {size_hello}\n")
            csv_file.flush()

        oardel([(job_id, site)])
        csv_file.close()

    def run(self):
        print("Done !")

if __name__ == "__main__":
    ENGINE = MyEngine()
    ENGINE.start()
