from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os

OAR_JOB_ID=None


class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_build_file', help='Path to the NXC deploy file')
        parser.add_argument('--build', action='store_true', help='Either to build the composition')
        parser.add_argument('--clean', action='store_true', help='Clean the store')
        parser.add_argument('--only-build', action='store_true', help='Build the composition and exits')
        parser.add_argument('--nxc_folder', default=f"{os.getcwd()}", help='Path to the nxc folder')
        parser.add_argument('--composition_name', help='name of the composition')
        parser.add_argument('--night', action='store_true', help='either to submit jobs at g5k night')
        parser.add_argument('--nix-chroot-script', help='path to the nix user chroot script, fetch from github otherwise')
        self.nodes = {}
        self.composition_name = "composition"

    def init(self):
        global OAR_JOB_ID
        nb_nodes = 1
        site = "grenoble"
        cluster = "dahu"
        job_time_type = "day"

        if self.args.composition_name:
            self.composition_name = self.args.composition_name
        if self.args.night:
            job_time_type = "night"

        nxc_build_file = None
        if self.args.build or self.args.only_build:
            (nxc_build_file, _time, _size) = build_nxc_execo(self.args.nxc_folder,
                                                             site, cluster,
                                                             walltime=15*60,
                                                             extra_job_type=[job_time_type],
                                                             composition_name=self.composition_name,
                                                             nix_chroot_script=self.args.nix_chroot_script,
                                                             clean_store=self.args.clean)
            print(f"Built in {_time} seconds, output of size {_size}")
            if self.args.only_build:
                sys.exit()
        elif self.args.nxc_build_file is not None:
            nxc_build_file = self.args.nxc_build_file
        else:
            raise Exception("No compose info file ...")

        print(nxc_build_file)
        oar_job = reserve_nodes(nb_nodes, site, cluster, walltime=15*60, job_time_type=job_time_type)
        oar_job_id, site = oar_job[0]
        OAR_JOB_ID=oar_job_id
        roles_quantities = {"foo": 1}
        self.nodes = get_oar_job_nodes_nxc(self.oar_job_id,
                                           site,
                                           compose_info_file=nxc_build_file,
                                           composition_name=self.composition_name,
                                           roles_quantities=roles_quantities)
        print(self.nodes)

    def run(self):
        my_command = "echo \"Hello from $(whoami) at $(hostname) ($(ip -4 addr | grep \"/20\" | awk '{print $2;}'))\" > /tmp/hello"
        hello_remote = Remote(my_command, self.nodes["foo"], connection_params={'user': 'root'})
        hello_remote.run()

        my_command2 = "cat /tmp/hello"
        cat_remote = Remote(my_command2, self.nodes["foo"], connection_params={'user': 'root'})
        cat_remote.run()
        for process in cat_remote.processes:
            print(process.stdout)

def reserve_nodes(nb_nodes, site, cluster, walltime=3600, job_time_type="day"):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["allow_classic_ssh", job_time_type]), site)])
    return jobs

if __name__ == "__main__":
    ENGINE = MyEngine()
    try:
        ENGINE.start()
    except Exception as ex:
        print(f"Failing with error {ex}")
    oardel([(OAR_JOB_ID, None)])
    print("Giving back the resources")
