{ pkgs, nxcEnv, ... }:

{
  run_expe_full = pkgs.writeScriptBin "run_expe_full" ''
    # TODO: clean store
    ${nxcEnv}/bin/python3 ${
      ./execo_script.py
    } --clean --only-build --composition_name empty --nix-chroot-script ${./nix-user-chroot.sh} $@
    ${nxcEnv}/bin/python3 ${
      ./execo_script.py
    } --only-build --composition_name hello --nix-chroot-script ${./nix-user-chroot.sh} $@
  '';
  run_expe_empty = pkgs.writeScriptBin "run_expe_empty" ''
    ${nxcEnv}/bin/python3 ${
      ./execo_script.py
    } --build --composition_name empty --nix-chroot-script ${./nix-user-chroot.sh} $@
  '';
  run_expe_hello = pkgs.writeScriptBin "run_expe_hello" ''
    ${nxcEnv}/bin/python3 ${
      ./execo_script.py
    } --build --composition_name hello --nix-chroot-script ${./nix-user-chroot.sh} $@
  '';
  run_clean_build_rebuild = pkgs.writeScriptBin "run_clean_build_rebuild" ''
    ${nxcEnv}/bin/python3 ${
      ./clean_build_and_rebuild.py
    } --nix-chroot-script ${./nix-user-chroot.sh} $@
  '';
}
