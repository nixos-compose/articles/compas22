{ pkgs, ... }: {
  nodes = {
    foo = { pkgs, ... }:
      {
        environment.systemPackages = with pkgs; [ hello ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
