{
  description = "nixos-compose - basic setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
  };

  outputs = { self, nixpkgs, nxc }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      nixos-compose = nxc.defaultPackage.${system};
      nxcEnv = nixos-compose.dependencyEnv;

      expes = import ./scripts/experiments.nix { inherit pkgs nxcEnv; };
    in {
      packages.${system} = (nxc.lib.compose {
        inherit nixpkgs system;
        compositions = ./compositions/compositions.nix;
      }) // expes;

      apps.${system} = builtins.mapAttrs (name: value: {
        type = "app";
        program = "${value}/bin/${name}";
      }) expes;

      devShell.${system} = pkgs.mkShell { buildInputs = [ nixos-compose ]; };
    };
}
