from execo import Process, SshProcess, Remote
from execo.action import Local
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep
from execo_g5k.kadeploy import deploy, Deployment
import execo_engine

import sys
import os
import os.path as op
import yaml
import shutil
import time

class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--night', action='store_true', help='either to submit jobs at g5k night')
        parser.add_argument('--recipe', help='yaml file')
        parser.add_argument('--iterations', default=1, help='nb of iters')

    def init(self):
        nb_nodes = 1
        site = "grenoble"
        cluster = "dahu"
        job_time_type = "day"

        self.current_path = os.getcwd()

        if self.args.night:
            job_time_type = "night"

        self.iterations = int(self.args.iterations)
        self.env = self.args.recipe

        oar_job = reserve_nodes(nb_nodes, site, cluster, walltime=2.5*60*60, job_time_type=job_time_type)
        self.oar_job_id, self.site = oar_job[0]


    def run(self):
        print(get_oar_job_nodes(self.oar_job_id, self.site))

        build_node = get_oar_job_nodes(self.oar_job_id, self.site)[0]

        deployed, undeployed = deploy(Deployment(hosts=[build_node], user='root', env_file=op.join(self.current_path, self.env)))

        add_nix_flakes_remote = Remote("echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf", build_node, connection_params={'user': 'root'})
        add_nix_flakes_remote.run()

        csv_file = open(os.path.join(self.current_path, f"data/data_{self.oar_job_id}.csv"), "w")
        csv_file.write("image, flavour, time, size\n")

        self.root_path = "/home/quentin"
        size = -1
        self.path_nix = "export PATH=\"$PATH:/nix/var/nix/profiles/default/bin\";"

        for flavour in ["g5k-ramdisk", "g5k-image"]:
            for i in range(self.iterations):
                execo_engine.log.logger.info(f"Starting iteration {i + 1}/{self.iterations}")

                execo_engine.log.logger.info("Building the 'empty' image")
                (name, duration) = self.build_and_get_stats("empty", flavour, build_node)
                csv_file.write(f"empty, {flavour}, {duration}, {size}\n")
                csv_file.flush()
                execo_engine.log.logger.info("Building the 'hello' image")
                (name, duration) = self.build_and_get_stats("hello", flavour, build_node)
                csv_file.write(f"hello, {flavour}, {duration}, {size}\n")
                csv_file.flush()

                execo_engine.log.logger.info("Removind the 'build' folder")
                Remote(f"{self.path_nix} rm result && nix-collect-garbage -d", build_node, connection_params={'user':'root'}).run()
        csv_file.close()

        execo_engine.log.logger.info("Giving back the resources")
        # oardel([(self.oar_job_id, self.site)])

    def build_and_get_stats(self, image_name, flavour, build_node):
        build_cmd = f"{self.path_nix} nix build {self.root_path}/#{image_name}::{flavour}"
        build_remote = Remote(build_cmd, build_node, connection_params={'user':'root'})
        start_time = time.time()
        build_remote.run()
        end_time = time.time()
        execo_engine.log.logger.info(f"Built {image_name} in {end_time - start_time}")
        return (image_name, end_time - start_time)

def reserve_nodes(nb_nodes, site, cluster, walltime=3600, job_time_type="day"):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=["deploy", "allow_classic_ssh", job_time_type]), site)])
    return jobs

def get_folder_size(folder):
	size = 0
	for path, dirs, files in os.walk(folder):
		for f in files:
			fp = os.path.join(path, f)
			size += os.stat(fp).st_size
	return size

if __name__ == "__main__":
    ENGINE = MyEngine()
    ENGINE.start()
