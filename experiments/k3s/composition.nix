{ pkgs, flavour, ... }:
let
  # A random token for mutual authentication
  k3sToken = "df54383b5659b9280aa1e73e60ef78fc";

  virtualisationOptions = { ... }:
    if flavour.name == "nixos-test" || flavour.name == "nixos-test-ssh" then {
      virtualisation.memorySize = 1536;
      virtualisation.diskSize = 4096;
    } else
      { };

  commonConfig = { pkgs, ... }: {
    imports = [ virtualisationOptions ];
    environment.systemPackages = with pkgs; [ k3s gzip ];
    environment.sessionVariables = { DEPLOY_FILE = "${./deployment.yaml}"; };
  };

in {
  nodes = {
    server = { pkgs, ... }: {
      imports = [ commonConfig ];
      networking.firewall.allowedTCPPorts = [ 6443 ];
      services.k3s = {
        enable = true;
        role = "server";
        extraFlags = "--token ${k3sToken}";
      };
    };

    agent = { pkgs, ... }: {
      imports = [ commonConfig ];
      services.k3s = {
        enable = true;
        role = "agent";
        serverAddr = "https://server:6443";
        token = k3sToken;
        # extraFlags = "--node-label app=node-js";
      };
    };
  };

  testScript = ''
    start_all()

    server.wait_for_unit("k3s")
    agent.wait_for_unit("k3s")

    server.succeed("k3s kubectl cluster-info")
  '';
}
