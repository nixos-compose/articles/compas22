{
  description = "nixos-compose - basic setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose";
  };

  outputs = { self, nixpkgs, nxc }:
    let
      system = "x86_64-linux";
      composition = ./composition.nix;
    in {
      packages.${system} =
        nxc.lib.compose { inherit nixpkgs system composition; };

      defaultPackage.${system} =
        self.packages.${system}."composition::nixos-test";

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;

    };
}
