{
  description = "A very basic flake";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05"; };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in {

      packages.${system} = {
        paper = pkgs.stdenv.mkDerivation {
          name = "compas22";
          src = ./paper;
          buildInputs = [ pkgs.texlive.combined.scheme-full pkgs.bibtool ];
          buildPhase = ''
            mkdir -p $out
            pdflatex main.tex
            bibtex main.aux
            pdflatex main.tex
            pdflatex main.tex
            cp main.pdf $out/
          '';
          installPhase = ''
            echo "Skipping installPhase"
          '';
        };

        journal = pkgs.stdenv.mkDerivation {
          name = "journal";
          src = ./.;
          buildInputs = [
            pkgs.rPackages.rmarkdown
            pkgs.rPackages.markdown
            pkgs.rPackages.knitr
            pkgs.pandoc
            pkgs.texlive.combined.scheme-full
            pkgs.R
          ];
          installPhase = ''
            mkdir -p $out
            Rscript -e 'rmarkdown::render("./journal.Rmd", "html_document")'
            mv ./journal.html $out/
          '';
        };
      };
      defaultPackage.${system} = self.packages.${system}.paper;
    };
}
