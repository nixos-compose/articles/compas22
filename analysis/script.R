library(tidyverse)
library(reshape2)
library(facetscales)

get_df_size <- function(filename) {
    read_csv(filename, col_names = T) %>%
        filter(name %in% c("image", "tarball")) %>%
        mutate(filename = basename(filename)) %>%
        separate(filename, sep = "_", into = c("flavour", "image")) %>%
        separate(image, into = c("image", "csv")) %>%
        mutate(type = paste("nxc", flavour, sep="-")) %>%
        select(name, size, type, image) %>%
        group_by(type, image) %>%
        summarise(image_size = max(size))
}

filenames_sizes <- list.files(path = "../experiments/empty_image/data/", pattern = "g5k-*", full.names = T)

df_sizes <- filenames_sizes %>%
    map_df(get_df_size)

empty_kameleon <- read_csv("./empty_nfs.csv", col_names = c("path", "image_size")) %>%
    separate(path, sep = "/", into = c("base", "file")) %>%
    filter(file == "empty_nfs.tar.zst") %>%
    mutate(type = "kameleon", image = "empty") %>%
    select(type, image, image_size)

hello_kameleon <- read_csv("./hello_nfs.csv", col_names = c("path", "image_size")) %>%
    separate(path, sep = "/", into = c("base", "file")) %>%
    filter(file == "hello_image.tar.zst") %>%
    mutate(type = "kameleon", image = "hello") %>%
    select(type, image, image_size)


df_sizes <- rbind(df_sizes, empty_kameleon, hello_kameleon)

df_sizes

df_nxc_no_nfs <- read_csv("../experiments/nix_without_nfs/data/data_2096959.csv", col_names = T) %>%
    mutate(type = paste("nxc-", flavour, sep="")) %>%
    dplyr::select(-flavour, -size) %>%
    rename(time_no_nfs = time)

df_nxc_image <- read_csv("../experiments/empty_image/data/data_2094234.csv", col_names = T) %>%
    dplyr::select(-flavour) %>%
    mutate(type = "nxc-g5k-image")

df_nxc_image <- left_join(df_nxc_image, df_nxc_no_nfs)

# df_nxc_ramdisk <- read_csv("../experiments/empty_image/data_ramdisk.csv", col_names = T) %>%
df_nxc_ramdisk <- read_csv("../experiments/empty_image/data/data_2094191.csv", col_names = T) %>%
    dplyr::select(-flavour) %>%
    mutate(type = "nxc-g5k-ramdisk")

df_nxc_ramdisk <- left_join(df_nxc_ramdisk, df_nxc_no_nfs)


df_kameleon <- read_csv("../experiments/kameleon/data.csv", col_names = T) %>%
    mutate(type = "kameleon") %>%
    separate(image, sep = "_", into = c("image", "foo")) %>%
    dplyr::select(-foo)

kameleon_no_nfs <- read_csv("../experiments/kameleon/data/data_2096985.csv", col_names = T) %>%
    rename(type = flavour) %>%
    dplyr::select(-size) %>%
    rename(time_no_nfs = time)

# kameleon_no_nfs

df_kameleon <- left_join(df_kameleon, kameleon_no_nfs)


df <- rbind(df_nxc_image, df_nxc_ramdisk, df_kameleon)

as.data.frame(df)

scales_x <- list(
  time = scale_x_continuous(),
  time_no_nfs = scale_x_continuous(),
  size = scale_x_continuous(),
  image_size = scale_x_continuous()
)



df <- df %>%
    inner_join(df_sizes) %>%
    mutate(
        size = size / (1024 * 1024 * 1024),
        image_size = image_size / (1024 * 1024),
    ) %>%
    melt(id.vars = c("image", "type")) %>%
    mutate(image = factor(image)) %>%
    mutate(image = fct_relevel(image, "hello", "empty")) %>%
    #mutate(across(variable = factor(variable, levels = c("time_no_nfs", "time", "size", "image_size")))) %>%
    group_by(image, type, variable) %>%
    summarise(mean_value = mean(value), bar = 2.576 * sd(value) / sqrt(n()))


df


max_time_with_nfs <- df %>%
    filter(variable == "time") %>%
    group_by(type, image) %>%
    summarise(mean_value = max(mean_value) + max(bar)) %>%
    pull(mean_value)

max_time_without_nfs <- df %>%
    filter(variable == "time_no_nfs") %>%
    group_by(type, image) %>%
    summarise(mean_value = max(mean_value) + max(bar)) %>%
    pull(mean_value)

df_blank <- crossing(
        tibble(image = c("hello", "empty")),
        tibble(variable = c("time", "time_no_nfs")),
        tibble(type = c("kameleon", "nxc-g5k-image", "nxc-g5k-ramdisk")),
        tibble(mean_value = c(max(max_time_without_nfs, max_time_with_nfs)))
    ) %>%
    mutate(type = factor(type), variable = factor(variable))

df_blank

df %>%
    ggplot(aes(x = type, fill = image, y = mean_value)) +
    #facet_wrap_sc(. ~ variable,
    geom_col(position = "dodge") +
    geom_blank(
        data = df_blank
    ) +
    geom_errorbar(
        data = . %>% filter(variable != "image_size" & !(variable %in% c("size") & type %in% c("nxc-g5k-image", "nxc-g5k-ramdisk"))),
        aes(ymin = mean_value - bar , ymax = mean_value + bar),
        width = 0.5,
        position=position_dodge(.9)
    ) +
    facet_wrap(. ~ variable,
               nrow = 2,
               # scales = "free_y",
               scales = "free_x",
               # scales = scales_x,
               strip.position = "top",
               labeller = as_labeller(c(time = "Temps de construction sur NFS [s]", size = "Taille des artefacts [Gio]", time_no_nfs = "Temps de construction sans NFS [s]", image_size = "Taille de l'image deployée [Mio]"))) +
    scale_fill_grey(
      # "Environments",
      name = NULL,
      start = 0.5,
      end = 0.8,
      breaks = c("empty", "hello"),
      labels = c("base", "base + hello")
    ) +
    ylim(0, NA) +
    ylab(NULL) +
    # xlab("Variantes") +
    xlab(NULL) +
    # ggtitle(
    #     "Temps de construction et taille des artéfacts",
    #     subtitle = "avec intervalles de confiance à 99 %"
    # ) +
    theme_bw() +
    theme(
        legend.position = "bottom",
        # legend.position = c(.9, .95),
        legend.box = "horizontal",
        strip.background = element_blank(),
        strip.placement = "outside",
        text = element_text(size = 20),
        # axis.text.y = element_text(angle = 45)
    ) +
    coord_flip()

ggsave("../paper/figs/eval_nxc_kameleon.pdf", width = 9.5, height = 5)
