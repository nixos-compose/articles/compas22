{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05";
    qornflakes.url = "github:GuilloteauQ/qornflakes";
  };

  outputs = { self, nixpkgs, qornflakes }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    qorn = qornflakes.packages.${system};
    rPkgs = with pkgs.rPackages; [
      tidyverse
      zoo
      reshape2
      treemapify
      qorn.facetscales
    ];

    myRStudio = pkgs.rstudioWrapper.override { packages = rPkgs; };
    myR = pkgs.rWrapper.override { packages = rPkgs; };

  in
  {
    devShell.${system} = pkgs.mkShell {
      buildInputs = [ myR myRStudio ];
    };
  };
}
